const fs = require("fs");
const util = require("util");

const materialIcons = require('@mdi/js');

const { toCamelCase, toSnakeCase } = require("js-convert-case");
const xmlParser = require("xml-js");

const svgo = new (require("svgo/lib/svgo"))({
  plugins: [
    { removeUselessStrokeAndFill: false },
    { removeXMLNS: true },
    { removeAttrs: { attrs: [ "data-.*", "aria.*" ] } },
    { convertStyleToAttrs: true },
  ],
});

function ocamlize(functionName, xmlJs) {
  function dekeyword(word) {
    const keywords = [
      "and",
      "as",
      "assert",
      "asr",
      "begin",
      "class",
      "constraint",
      "do",
      "done",
      "downto",
      "else",
      "end",
      "exception",
      "external",
      "false",
      "for",
      "fun",
      "function",
      "functor",
      "if",
      "in",
      "include",
      "inherit",
      "initializer",
      "land",
      "lazy",
      "let",
      "lor",
      "lsl",
      "lsr",
      "lxor",
      "match",
      "method",
      "mod",
      "module",
      "mutable",
      "new",
      "nonrec",
      "object",
      "of",
      "open",
      "or",
      "private",
      "rec",
      "sig",
      "struct",
      "then",
      "to",
      "true",
      "try",
      "type",
      "val",
      "virtual",
      "when",
      "while",
      "with",
    ];

    return keywords.includes(word) ? word + "'" : word;
  }

  function mapAttributes(attributes) {
    return Object.keys(attributes || {}).map(
      (key) => `A.${dekeyword(toCamelCase(key))} "${attributes[key]}"`
    );
  }

  function mapElement(element) {
    const name = element.name;
    const attributes = mapAttributes(element.attributes);
    const elements = (element.elements || []).map((element) =>
      mapElement(element)
    );

    return `E.${name} [ ${attributes.join("; ")} ] [ ${elements.join("; ")} ]`;
  }

  const svgElement = xmlJs.elements[0];
  const svgAttributes = mapAttributes(svgElement.attributes);
  const vdom = svgElement['elements'].map(mapElement);

  return `let ${dekeyword(toSnakeCase(functionName))} properties =
  let module E = Tea.Svg in
  let module A = Tea.Svg.Attributes in
  E.svg ([ ${svgAttributes.join("; ")} ] @ properties) [ ${vdom.join("; ")} ]`;
}

(async () => {
  const icons = await Promise.all(
    Object.keys(materialIcons).filter((name) => name.startsWith("mdi")).map((name) =>
      (async () => {
        const data = '<?xml version="1.0" encoding="UTF-8"?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" fill="currentColor" width="24" height="24" viewBox="0 0 24 24"><path d="' + materialIcons[name] + '" /></svg>';
        return await svgo.optimize(data).then(({ data, info }) => {
          const xmlJs = xmlParser.xml2js(data);
          const ocaml = ocamlize(name.substring(3), xmlJs);

          return {
            data,
            xmlJs,
            ocaml,
          };
        });
      })()
    )
  );

  fs.mkdir("./src", {}, () => {
    fs.writeFile(
      "./src/materialIcons.ml",
      icons.map((icon) => icon.ocaml).join("\n\n"),
      (err) => {
        if (err) throw err;
      }
    );
  });
})();
