Release 0.0.2
-------------
Add fill color

Release 0.0.1
-------------
Initial release to automatically import Material Design Icons and create bucklescript bindings.
